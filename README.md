Password Generator

This is a python script that generates a random password based on a few criteria: 
-Password length between 8-12 characters
-Uses upper and lowercase letters (A-Z), digits and symbols. 
It uses the random and string modules to randomly select the characters for the password using a loop. 
The script also contains unittesting to verify that the generated password meets the expected criteria. 

Nabil has updated the script:

- The user can now choose how many characters the password should be (between 8-12)
- Multiple passwords can be generated at once
- Does not give an error if the password only contains letters 

To generate a password, all you need to do is to execute the script and the password will be printed to the console. 

Nabil; Du kan vidareutveckla koden hur du vill, jag hade tänkt att användaren själv kan välja hur många lösenord som ska skapas eller att användaren själv får välja kriteria för lösenordet. En annan idé kan vara att skapa en Password strength indicator som inte bara skapar lösenordet men också utvärderar hur starkt lösenordet är baserat på faktorer som längd, komplexitet av karaktärer osv. En till idé kan vara att varje lösenord som skapas sparas ner i en fil. 


.gitlab-ci.yml

pycodestyle is used to perform linting checks via PEP8
pytest is used to run automated unit testing as part of the CI/CD pipeline
the .gitlab-ci.yml file also includes a build stage to create docker image and docker-in-docker service (docker:dind)
It then pushes the image to the gitlab container registry using "docker push". 

PAT: glpat-9dZBiUzy5oy8NyTPg8X7
