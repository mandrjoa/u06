# Use the official Python image as the base image
FROM python:3.9

# Set the working directory in the container
WORKDIR /app

# Copy the Python files to the working directory
COPY . .

# Install pytest for running unit tests
RUN pip install --no-cache-dir pytest

# Run unit tests
RUN pytest

# Define the command to run when the container starts
CMD ["python", "password_generator.py"]
