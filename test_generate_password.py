import pytest
import string
from password_generator import generate_password


def test_generate_password():
    # Testa att lösenordet är mellan 8 och 12 tecken långt
    password = generate_password()
    assert len(password) >= 8
    assert len(password) <= 12

    # Testa att lösenordet innehåller åtminstone en liten bokstav
    assert any(c.islower() for c in password)

    # Testa att lösenordet innehåller åtminstone en stor bokstav
    assert any(c.isupper() for c in password)

    # Testa att lösenordet innehåller åtminstone en siffra
    assert any(c.isdigit() for c in password)

    # Testa att lösenordet har åtminstone en symbol
    assert any(c in string.punctuation for c in password)
